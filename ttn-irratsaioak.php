<?php
/**
 * Plugin Name: TTN Irratsaioak
 * Description: Irratsaioen zerrenda.
 * Version: 0.0.4
 * Author: Jimakker
 * Author URI: http://twitter.com/jimakker
 */


require_once('lib/custom_post.php');

add_action( 'widgets_init', 'ttn_irratsaioak' );


function ttn_irratsaioak() {
	register_widget( 'Ttn_Irratsaioak' );
}

class Ttn_Irratsaioak extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'ttn-irratsaioak', 'description' => __('Irratsaioen zerrenda ', 'ttn-irratsaioak') );

		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'ttn-irratsaioak-widget' );

		parent::__construct( 'ttn-irratsaioak-widget', __('TTN Irratsaioak', 'ttn-irratsaioak'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
		$limit = $instance['limit'];
		$show_info = isset( $instance['show_info'] ) ? $instance['show_info'] : false;

		echo $before_widget;

		// Display the widget title
		if ( $title )
			echo '<h2 style="color:#CC0A0A;">' . $title . '</h2>';
		if ( !$limit )
			$limit = 10;


		$args = array( 'post_type' => 'irratsaioa', 'posts_per_page' => -1, 'meta_key' => 'on_air', 'meta_value' => 1, 'orderby' => 'title', 'order' => 'ASC' );
		$loop = new WP_Query( $args );
		?>
		<h3 style="background:#222;width:100%;color:#fff;text-align:center;margin:0;">IRRATSAIOAK</h3>
		<div class="list-group" style="padding:5px;background:#555;">

		<?php
		while ( $loop->have_posts() ) : $loop->the_post();
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()),'thumbnail');
		?>

			<div class="media" style="margin-top:5px;padding-bottom:2px;border-bottom:1px solid #333;">
			  <a class="pull-left" href="<?php echo get_permalink();?>">
			    <img class="media-object" width="35" src="<?php echo $thumb[0];?>" alt="<?php the_title();?>">
			  </a>
			  <div class="media-body" style="line-height:1.2;">
			  		<a style="color:#eee;"href="<?php echo get_permalink();?>">
			    		<?php the_title();?>
			    	</a>
			    <br/>
				<?php
				 $irr_kat = get_field('irratsaio_kategoria',get_the_ID(),true);
				 $kat_url =  get_term_link($irr_kat[0],null);
				 ?>
				<a href="<?php echo $kat_url;?>" style="color:#aaa;"><small>berriak</small></a> <small>|</small> <a href="<?php echo $kat_url;?>/feed" target="_blank" style="color:#aaa;"><small>rss</small></a>

			  </div>
			</div>

			<?php
		endwhile;

		?>

		</div>



		<?php
		echo $after_widget;
	}

	//Update the widget

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['limit'] = strip_tags( $new_instance['limit'] );
		// $instance['show_info'] = $new_instance['show_info'];

		return $instance;
	}


	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'title' => __('TTN Irratsaioak', 'example'), 'limit' => __('10', 'example'), 'show_info' => true );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'example'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

	<?php
	}
}

?>
