<?php

if ( ! function_exists('irratsaioak_post_type') ) {

// Register Custom Post Type
function irratsaioak_post_type() {

  $labels = array(
    'name'                => _x( 'Irratsaioak', 'Post Type General Name', 'text_domain' ),
    'singular_name'       => _x( 'Irratsaioa', 'Post Type Singular Name', 'text_domain' ),
    'menu_name'           => __( 'Irratsaioak', 'text_domain' ),
    'parent_item_colon'   => __( 'Irratsaio gurasoa', 'text_domain' ),
    'all_items'           => __( 'Irratsaio guztiak', 'text_domain' ),
    'view_item'           => __( 'Irratsaioa ikusi', 'text_domain' ),
    'add_new_item'        => __( 'Irratsaioa gehitu', 'text_domain' ),
    'add_new'             => __( 'Irratsaio berria', 'text_domain' ),
    'edit_item'           => __( 'Irratsaioa editatu', 'text_domain' ),
    'update_item'         => __( 'Irratsaioa eguneratu', 'text_domain' ),
    'search_items'        => __( 'Irratsaioak bilatu', 'text_domain' ),
    'not_found'           => __( 'Ez da irratsaiorik aurkitu', 'text_domain' ),
    'not_found_in_trash'  => __( 'Ez dago irratsaiorik zaborrontzian', 'text_domain' ),
  );
  $rewrite = array(
    'slug'                => 'irratsaioak',
    'with_front'          => true,
    'pages'               => true,
    'feeds'               => true,
  );
  $args = array(
    'label'               => __( 'irratsaioa', 'text_domain' ),
    'description'         => __( 'Irratsaioen informazio orriak', 'text_domain' ),
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => '',
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'rewrite'             => $rewrite,
    'capability_type'     => 'page',
  );
  register_post_type( 'irratsaioa', $args );

}

// Hook into the 'init' action
add_action( 'init', 'irratsaioak_post_type', 0 );

}



?>